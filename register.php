<?php
/**
 * Todo: add caching and refactor whole file, currently one heavy-duty function
 */

use Flood\Captn;
use Doctrine\Common\Annotations\AnnotationRegistry;

if (function_exists('captnIsSteering') && captnIsSteering()) {
    Captn\Acknowledge::signal(
        'flood.sonar',
        'root.flood.sonar',
        Captn::TYPE__COMPONENT,
        [
            'annotation' => [
                // dir is handled as psr-4 silently failing loader
                'psr4' => [
                    [
                        'namespace' => 'Flood\Component\Sonar\Annotation\\',
                        'dir'       => __DIR__ . '/src/Annotation/',
                    ],
                ],
                /*'loader' => static function ($class) {

                     // The annotation loaded can also be set as loader, see Doctrine Docs

                },
                'scan'   => [
                    // which folders should be scanned
                    [
                        'ext'     => 'php',
                        'include' => __DIR__ . '/',
                        'exclude' => __DIR__ . '/Annotation',
                    ],
                ],*/
            ],
        ]
    );

    Captn\EventDispatcher::on(
        'flood.parseAnnotation',
        static function ($sonar) {
            /**
             * @var \Flood\Component\Sonar $sonar
             */

            //
            // Build the needed information from acknowledged things
            $psr4_list = [];
            $loader_list = [];
            $scan_list = [];

            Captn\Acknowledge::onEachSignaled(static function ($value) use (&$psr4_list, &$loader_list, &$scan_list) {
                if (isset($value['annotation'])) {
                    if (isset($value['annotation']['psr4']) && is_string($value['annotation']['psr4'])) {
                        $psr4_list[] = $value['annotation']['psr4'];
                    } else if (isset($value['annotation']['psr4'])) {
                        foreach ($value['annotation']['psr4'] as $dir) {
                            $psr4_list[] = $dir;
                        }
                    }
                    if (isset($value['annotation']['loader'])) {
                        $loader_list[] = $value['annotation']['loader'];
                    }
                    if (isset($value['annotation']['scan'])) {
                        $scan_list[] = $value['annotation']['scan'];
                    }
                }
            });

            //
            // Build and assign needed autoloaders
            foreach ($psr4_list as $psr4) {
                $loader_list[] = static function ($class) use ($psr4) {
                    if (strpos($class, $psr4['namespace']) === 0) {
                        // be sure to that it fails silently
                        $file = $psr4['dir'] . str_replace("\\", DIRECTORY_SEPARATOR, str_replace($psr4['namespace'], '', $class)) . '.php';

                        if (file_exists($file)) {
                            require_once $file;

                            return true;
                        }
                    }
                };
            }

            foreach ($loader_list as $loader) {
                AnnotationRegistry::registerLoader($loader);
            }

            //
            // Get all needed files that will be scanned for their annotations
            $scan_dir_compiled = [];

            foreach ($scan_list as $scan_l) {
                foreach ($scan_l as $scan_dir) {
                    // returns an array of files in which the hydro classes are
                    if (is_string($scan_dir['include'])) {
                        $scan_dir['include'] = [$scan_dir['include']];
                    }
                    if (!isset($scan_dir['exclude'])) {
                        $scan_dir['exclude'] = [];
                    } else if (is_string($scan_dir['exclude'])) {
                        $scan_dir['exclude'] = [$scan_dir['exclude']];
                    }

                    if (is_string($scan_dir['ext'])) {
                        $scan_dir['ext'] = [$scan_dir['ext']];
                    }

                    foreach ($scan_dir['include'] as $incl_dir) {

                        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($incl_dir), \RecursiveIteratorIterator::SELF_FIRST);

                        foreach ($iterator as $file) {
                            if (!$file->isFile()) {
                                // todo: implement breakout if the folder should not be searched, currently searching `.git`
                                /*if (in_array($file->getFilename(), $exclude) || in_array(str_replace(__DIR__, '', $file->getPathname()), $exclude)) {
                                    continue;
                                } else {
                                    var_dump(str_replace(__DIR__, '', $file->getPathname()));
                                    var_dump($file->getFilename());
                                }*/
                            }
                            if ($file->isFile() && in_array($file->getExtension(), $scan_dir['ext'])) {
                                // todo: just a tmp dir filter
                                $in_forbiddenfolder = false;
                                foreach ($scan_dir['exclude'] as $excl_dir) {
                                    if (0 === strpos($file->getPathname(), $excl_dir)) {
                                        $in_forbiddenfolder = true;
                                    }
                                }
                                if (!$in_forbiddenfolder) {
                                    $scan_dir_compiled[] = $file->getPathname();
                                }
                            }
                        }
                    }
                }
            }

            foreach ($scan_dir_compiled as $php_file) {
                $sonar->analyzeFile($php_file);
            }
        }
    );
}