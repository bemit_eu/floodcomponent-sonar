<?php

namespace Flood\Component;

use Flood\Component\Sonar as S;

class Sonar {
    const TYPE__UNDEFINED = -1;
    const TYPE__CLASS = 0;
    const TYPE__CLASS_METHOD = 10;
    const TYPE__PROPERTY = 20;

    protected $config = [];

    /**
     * @var null|S\Reader
     */
    public $reader = null;

    /**
     * @var null|S\Storage
     */
    public static $storage = null;

    public function __construct($config, $cacher) {
        $this->config = $config;

        if(null === static::$storage) {
            static::$storage = new S\Storage();
        }

        $this->reader = new S\Reader($cacher, $this->config);
        /*$this->reader = new CachedReader(
            new IndexedReader(new AnnotationReader()),
            $cacher,
            (isset($this->config['debug']) ? $this->config['debug'] : false)
        );*/
    }

    public function analyze($callable, $hint = self::TYPE__UNDEFINED) {
        switch(gettype($callable)) {
            case 'string':
                // can be -function- no function is supported
                // can be class
                static::$storage->addClass($this->reader->getClass($callable));
                break;
            case 'array':
                error_log('Flood Sonar: type array, nothing to analyze');
                break;
            case 'object':
                // can be closure
                // can be class
                break;
            case 'resource':
                error_log('Flood Sonar: resource type is not implemented on analyze');
                break;
            default:
                error_log('Flood Sonar: could not determine type on analyze: ' . $callable);
                break;
        }
    }

    /**
     * Receives the path to a file and file read the content and parse it with a PHP tokenizer
     * Finds classes, builds the fully qualified class name `fq_cn`
     *
     * @todo: add component caching
     *
     * @param string $code
     */
    public function analyzeCode($code) {
        // todo: moving tokenizer to an extra object?
        $token = token_get_all($code, TOKEN_PARSE);
        $token_qty = count($token);
        $i = 0;
        $type = false;
        // namespace
        $namespace = '';
        // class list of all classes in file
        $class_list = [];
        for(; $i < $token_qty; $i++) {
            if(T_NAMESPACE === $token[$i][0]) {
                $namespace = '';
                $type = T_NAMESPACE;
            }
            if(T_CLASS === $token[$i][0]) {
                $class_list[] = '';
                $class_current = &$class_list[count($class_list) - 1];
                $type = T_CLASS;
            }
            /*var_dump($token[$i]);
            if (isset($token[$i][0]) && is_int($token[$i][0])) {
                var_dump(token_name($token[$i][0]));
            }*/
            if(false !== $type) {
                for($j = $i + 1; $j < $token_qty; $j++) {
                    /*var_dump('$j$j$j$j$j$j$j');
                    var_dump($token[$j]);
                    if (isset($token[$j][0]) && is_int($token[$j][0])) {
                        var_dump(token_name($token[$j][0]));
                    }*/
                    if(T_NAMESPACE === $type) {
                        if(';' === $token[$j]) {
                            $i = $j;
                            $type = false;
                            break;
                        }
                        if(isset($token[$j][1])) {
                            $namespace .= $token[$j][1];
                        } else {
                            $namespace .= $token[$j];
                        }
                    }
                    if(T_CLASS === $type) {
                        $string_found = false;
                        if(isset($token[$j][1]) && T_STRING === $token[$j][0]) {
                            if('__construct' === $token[$j][1]) {
                                // anonymous class detected, delete it
                                unset($class_list[count($class_list) - 1]);
                                break;
                            } else {
                                $class_current .= $token[$j][1];
                                $string_found = true;
                            }
                        } else if(T_STRING === $token[$j][0]) {
                            $class_current .= $token[$j];
                            $string_found = true;
                        }
                        if($string_found) {
                            $i = $j;
                            $type = false;
                            break;
                        }
                    }
                }
            }
        }
        //var_dump('$nmsp: ' . $nmsp);
        //var_dump($cls);
        foreach($class_list as $c) {
            $fq_cn = trim($namespace) . '\\' . trim($c);
            //var_dump($fq_cn_name);
            $this->analyze($fq_cn);
        }

        //$this->read($fq_cn_name);
    }

    public function analyzeFile($php_file) {
        if(is_file($php_file)) {
            $this->analyzeCode(file_get_contents($php_file));
        } else {
            error_log('Flood Sonar: file to analyze not found in analyzeFile: ' . $php_file);
        }
    }
}