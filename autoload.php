<?php

spl_autoload_register(function ($class) {
    if ('Flood\Component\Sonar' === $class) {
        require __DIR__ . '/Sonar.php';
    }
});