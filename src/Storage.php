<?php

namespace Flood\Component\Sonar;


class Storage {
    protected $annotation_list = [];
    protected $indexed_annotation_list = [];

    public function __construct() {
    }

    /**
     * @param array|string $class when array index `0` is used as class and index `1` as annotation
     * @param string|bool $annotation
     *
     * @return array
     */
    public function addClass($class, $annotation = false) {
        if (is_array($class) && !empty($class)) {
            $this->annotation_list[$class[0]] = $class[1];
            return $class;
        } else if (is_string($class) && false !== $annotation) {
            $this->annotation_list[$class] = $annotation;
            return [$class, $annotation];
        } else {
            return [];
        }
    }

    /**
     * @todo currently fails silently
     *
     * @param $annotation_class
     *
     * @return array|mixed
     */
    public function search($annotation_class) {
        if (isset($this->indexed_annotation_list[$annotation_class])) {
            return $this->indexed_annotation_list[$annotation_class];
        } else {
            foreach ($this->annotation_list as $class => $annotation) {
                foreach ($annotation as $an_class => $an) {
                    if ($annotation_class === $an_class) {
                        if (!isset($this->indexed_annotation_list[$an_class])) {
                            $this->indexed_annotation_list[$an_class] = [];
                        }
                        $this->indexed_annotation_list[$an_class][$class] = $an;
                    }
                }
            }
        }
        if (isset($this->indexed_annotation_list[$annotation_class])) {
            return $this->indexed_annotation_list[$annotation_class];
        } else {
            return [];
        }
    }
}