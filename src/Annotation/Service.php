<?php

namespace Flood\Component\Sonar\Annotation;

/**
 * Annotation for a container service object
 *
 * @Annotation
 *
 * @Target({"CLASS", "METHOD", "PROPERTY"})
 */
class Service {
    public $id;
    /**
     * @var bool `true` = calling callable on every `get($id)`, `false` = calling callable one time
     */
    public $executed = true;
}