<?php

namespace Flood\Component\Sonar;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Annotations\IndexedReader;

class Reader {
    protected $reader;
    protected $config;

    public function __construct($cacher, $config) {
        $this->config = $config;

        $this->reader = new CachedReader(
            new IndexedReader(new AnnotationReader()),
            $cacher,
            (isset($this->config['debug']) ? $this->config['debug'] : false)
        );
    }

    public function getClass($class_name) {
        // $annotation = $this->reader->getClassAnnotation(new \ReflectionClass($class_name),'ano_name');
        $reflection = new \ReflectionClass($class_name);
        $annotation = $this->reader->getClassAnnotations($reflection);
        // var_dump($annotation);
        if(is_array($annotation) && !empty($annotation)) {
            if(is_object($class_name)) {
                $class_name_str = get_class($class_name);
            } else {
                $class_name_str = $class_name;
            }

            return [$class_name_str, $annotation];
        }

        return [];
    }
}